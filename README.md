# Online Forum

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Link of Application](#link)
* [General Information ](#information)

## General info
a) As a user, can post a question to the Online Forum, so that I can get my question answered.
b) As a user, can post a comment on a question, so that I can participate in the conversation.
c) As a user, can post a comment on another user's comment so that I can have a side discussion.
	
## Technologies
Project is created with:
* Ruby version: 2.5.1
* Rails version: 5.2.3
* Using rvm for managing ruby version
	
## link
To run this project, install it locally using npm:

* heroku - https://online-forum.herokuapp.com/
* Bitbucket - https://bitbucket.org/avi4791/online_forum/src/master/
* Instruction - https://bitbucket.org/avi4791/online_forum/src/master/README.md

## information 

* In Landing page of the website you can see the posts posted by users
* Sign in, Sign up and create Post option given in the landing page.
* After sign in or sign up you will able to create the post as well as you will able to add comment to the posts posted by users.
* You can view the post and its comment in details by clickig on post title	
