Rails.application.routes.draw do
	devise_for :users, controllers: {
      sessions: 'users/sessions',
      registrations: 'users/registrations'
  }
	resources :posts do
		resources :comments
	end
	resources :comments do
		resources :comments
	end

  devise_scope :user do
    authenticated :user do
   
       get '/users/sign_out' => 'devise/sessions#destroy'
       root 'posts#index', as: :authenticated_root
       # root 'devise/registrations#edit', as: :authenticated_root
    end
    unauthenticated do
      root 'posts#index', as: :unauthenticated_root
      # root 'users/sessions#new', as: :unauthenticated_root
    end
  end	
end
